﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NerdDinner_2.Models;

namespace NerdDinner_2.Controllers
{
    public class DinnersController : Controller
    {
        //
        // GET: /Dinners/

        /*
        public ActionResult Index()
        {
            return View();
        }*/

        DinnerRepository d = new DinnerRepository();

        public void Index()
        {
            //Response.Write("<h1>Coming Soon: Dinners</h1>");
            var dinners = d.FindUpcomingDinners().ToList();
        }

        public void Details(int id)
        {
            Dinner dinner = d.GetDinner(id);
            Response.Write(dinner.Title);
            //Response.Write("<h1>Details DinnerID: " + id + "</h1>");
        }

    }
}
