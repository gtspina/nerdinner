﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NerdDinner_2.Models;

namespace NerdDinner_2.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /Test/

        public void Index()
        {
            Response.Write("<h1>....</h1>");

            /*
NerdDinnerDataContext db = new NerdDinnerDataContext();

Dinner dinner = db.Dinners.Single(d => d.DinnerID == 1);

dinner.Title = "Changed Title";
dinner.Description = "This dinner will be fun";

db.SubmitChanges();
            

DinnerRepository dinnerRepository = new DinnerRepository();
            
Dinner nd1 = new Dinner();
nd1.Title = "Title Event Test";
nd1.HostedBy = "Title Event HostedBy";
nd1.ContactPhone = "0000-0000";
nd1.EventDate = DateTime.Now;
nd1.Description = "Description";
nd1.Address = "Adress Test";
nd1.Country = "Country Test";

dinnerRepository.Add(nd1);
            
dinnerRepository.Save();
            

DinnerRepository dinnerRepository = new DinnerRepository();

Dinner myDinner = dinnerRepository.GetDinner(6);
myDinner.Title = "Updated Title";
dinnerRepository.Save();

DinnerRepository dr = new DinnerRepository();

Dinner myDinner = dr.GetDinner(7);

if (myDinner != null)
    dr.Delete(myDinner);

dr.Save();
*/
            DinnerRepository dr = new DinnerRepository();

            Dinner d = dr.GetDinner(6);
            d.Title = null;
            d.Description = null;
            d.HostedBy = null;

            if (!d.IsValid)
            {
                var errors = d.GetRuleViolations();

                foreach (var error in errors)
                    Response.Write("<h1>"+error.PropertyName + " - " + error.ErrorMessage+"</h1>");
            }
        }

    }
}
