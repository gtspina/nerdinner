﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NerdDinner_2.Models;

namespace NerdDinner_2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NerdDinnerDataContext db = new NerdDinnerDataContext();
/*
            Dinner dinner = db.Dinners.Single(d => d.DinnerID == 1);

            
            RSVP myRSVP = new RSVP();

            myRSVP.AttendeeName = "TestName";

            dinner.RSVPs.Add(myRSVP);*/

            Dinner dinner = db.Dinners.Single(d => d.DinnerID == 1);

            dinner.Title = "Changed Title";
            dinner.Description = "This dinner will be fun";

            db.SubmitChanges();
        }
    }
}